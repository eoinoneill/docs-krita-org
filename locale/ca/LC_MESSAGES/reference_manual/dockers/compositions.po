# Translation of docs_krita_org_reference_manual___dockers___compositions.po to Catalan
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: reference_manual\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-05-17 15:56+0200\n"
"Last-Translator: Antoni Bella Pérez <antonibella5@yahoo.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.04.1\n"

#: ../../<generated>:1
msgid "Rename composition"
msgstr "Reanomena la composició"

#: ../../<rst_epilog>:4
msgid ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: mouseright"
msgstr ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: mouseright"

#: ../../reference_manual/dockers/compositions.rst:1
msgid "Overview of the compositions docker."
msgstr "Vista general de l'acoblador Composicions."

#: ../../reference_manual/dockers/compositions.rst:12
#: ../../reference_manual/dockers/compositions.rst:17
msgid "Compositions"
msgstr "Composicions"

#: ../../reference_manual/dockers/compositions.rst:19
msgid ""
"The compositions docker allows you to save the configurations of your layers "
"being visible and invisible, allowing you to save several configurations of "
"your layers."
msgstr ""

#: ../../reference_manual/dockers/compositions.rst:22
msgid ".. image:: images/dockers/Composition-docker.png"
msgstr ".. image:: images/dockers/Composition-docker.png"

#: ../../reference_manual/dockers/compositions.rst:24
msgid "Adding new compositions"
msgstr ""

#: ../../reference_manual/dockers/compositions.rst:24
msgid ""
"You do this by setting your layers as you wish, then pressing the plus sign. "
"If you had a word in the text-box to the left, this will be the name of your "
"new composition."
msgstr ""

#: ../../reference_manual/dockers/compositions.rst:26
msgid "Activating composition"
msgstr ""

#: ../../reference_manual/dockers/compositions.rst:27
msgid "Double-click the composition name to switch to that composition."
msgstr ""

#: ../../reference_manual/dockers/compositions.rst:28
msgid "Removing compositions"
msgstr ""

#: ../../reference_manual/dockers/compositions.rst:29
msgid "The minus sign. Select a composition, and hit this button to remove it."
msgstr ""

#: ../../reference_manual/dockers/compositions.rst:30
msgid "Exporting compositions"
msgstr ""

#: ../../reference_manual/dockers/compositions.rst:31
msgid "The file sign. Will export all checked compositions."
msgstr ""

#: ../../reference_manual/dockers/compositions.rst:32
msgid "Updating compositions"
msgstr ""

#: ../../reference_manual/dockers/compositions.rst:33
msgid ""
"|mouseright| a composition to overwrite it with the current configuration."
msgstr ""

#: ../../reference_manual/dockers/compositions.rst:35
msgid "|mouseright| a composition to rename it."
msgstr ""
