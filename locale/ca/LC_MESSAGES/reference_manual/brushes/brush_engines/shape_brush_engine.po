# Translation of docs_krita_org_reference_manual___brushes___brush_engines___shape_brush_engine.po to Catalan
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: reference_manual\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-05-10 21:22+0200\n"
"Last-Translator: Antoni Bella Pérez <antonibella5@yahoo.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.04.0\n"

#: ../../<generated>:1
msgid "Hard Edge"
msgstr "Vora sòlida"

#: ../../reference_manual/brushes/brush_engines/shape_brush_engine.rst:1
msgid "The Shape Brush Engine manual page."
msgstr "La pàgina del manual per al motor del pinzell de formes."

#: ../../reference_manual/brushes/brush_engines/shape_brush_engine.rst:11
#: ../../reference_manual/brushes/brush_engines/shape_brush_engine.rst:16
msgid "Shape Brush Engine"
msgstr "Motor del pinzell de formes"

#: ../../reference_manual/brushes/brush_engines/shape_brush_engine.rst:11
msgid "Brush Engine"
msgstr "Motor de pinzell"

#: ../../reference_manual/brushes/brush_engines/shape_brush_engine.rst:11
msgid "Experiment Brush Engine"
msgstr "Motor del pinzell per experimentar"

#: ../../reference_manual/brushes/brush_engines/shape_brush_engine.rst:11
msgid "Al.Chemy"
msgstr "Al.Chemy"

#: ../../reference_manual/brushes/brush_engines/shape_brush_engine.rst:19
msgid ".. image:: images/icons/shapebrush.svg"
msgstr ".. image:: images/icons/shapebrush.svg"

#: ../../reference_manual/brushes/brush_engines/shape_brush_engine.rst:20
msgid "An Al.chemy inspired brush-engine. Good for making chaos with!"
msgstr "Un motor de pinzell inspirat en «Al.chemy». Bo per a crear el caos!"

#: ../../reference_manual/brushes/brush_engines/shape_brush_engine.rst:23
msgid "Parameters"
msgstr "Ajustaments"

#: ../../reference_manual/brushes/brush_engines/shape_brush_engine.rst:25
msgid ":ref:`option_experiment`"
msgstr ":ref:`option_experiment`"

#: ../../reference_manual/brushes/brush_engines/shape_brush_engine.rst:26
msgid ":ref:`blending_modes`"
msgstr ":ref:`blending_modes`"

#: ../../reference_manual/brushes/brush_engines/shape_brush_engine.rst:31
msgid "Experiment Option"
msgstr "Opció experimental"

#: ../../reference_manual/brushes/brush_engines/shape_brush_engine.rst:33
msgid "Speed"
msgstr "Velocitat"

#: ../../reference_manual/brushes/brush_engines/shape_brush_engine.rst:34
msgid ""
"This makes the outputted contour jaggy. The higher the speed, the jaggier."
msgstr ""
"Farà que el contorn resultant sigui irregular. Com més gran sigui la "
"velocitat, més irregular serà."

#: ../../reference_manual/brushes/brush_engines/shape_brush_engine.rst:35
msgid "Smooth"
msgstr "Suavitzat"

#: ../../reference_manual/brushes/brush_engines/shape_brush_engine.rst:36
msgid ""
"Smoothens the output contour. This slows down the brush, but the higher the "
"smooth, the smoother the contour."
msgstr ""
"Suavitzarà el contorn resultant. Això frenarà el pinzell, però com més gran "
"sigui el suavitzat, més suau serà el contorn."

#: ../../reference_manual/brushes/brush_engines/shape_brush_engine.rst:37
msgid "Displace"
msgstr "Desplaçament"

#: ../../reference_manual/brushes/brush_engines/shape_brush_engine.rst:38
msgid ""
"This displaces the shape. The slow the movement, the higher the displacement "
"and expansion. Fast movements shrink the shape."
msgstr ""
"Desplaçarà la forma. Com més lent sigui el moviment, més gran serà el "
"desplaçament i l'expansió. Els moviments ràpids faran encongir la forma."

#: ../../reference_manual/brushes/brush_engines/shape_brush_engine.rst:39
msgid "Winding Fill"
msgstr "Emplenat sinuós"

#: ../../reference_manual/brushes/brush_engines/shape_brush_engine.rst:40
msgid ""
"This gives you the option to use a 'non-zero' fill rules instead of the "
"'even-odd' fill rule, which means that where normally crossing into the "
"shape created transparent areas, it now will not."
msgstr ""
"Dóna l'opció d'utilitzar les regles de farciment «diferent de zero» en lloc "
"de la regla de l'emplenat «parell imparell», el qual vol dir quan normalment "
"es creua dins d'àrees transparents creades per la forma, ara deixaria de fer-"
"ho."

#: ../../reference_manual/brushes/brush_engines/shape_brush_engine.rst:42
msgid "Removes the anti-aliasing, to get a pixelized line."
msgstr "Elimina l'antialiàsing per obtenir una línia pixelada."
