msgid ""
msgstr ""
"Project-Id-Version: kdeorg\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-05-05 22:27\n"
"Last-Translator: Guo Yunhe (guoyunhe)\n"
"Language-Team: Chinese Simplified\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: crowdin.com\n"
"X-Crowdin-Project: kdeorg\n"
"X-Crowdin-Language: zh-CN\n"
"X-Crowdin-File: /kf5-trunk/messages/www/"
"docs_krita_org_reference_manual___brushes___brush_engines___filter_brush_engine."
"pot\n"

#: ../../reference_manual/brushes/brush_engines/filter_brush_engine.rst:1
msgid "The Filter Brush Engine manual page."
msgstr "Krita 的滤镜笔刷引擎介绍。"

#: ../../reference_manual/brushes/brush_engines/filter_brush_engine.rst:12
#: ../../reference_manual/brushes/brush_engines/filter_brush_engine.rst:17
msgid "Filter Brush Engine"
msgstr "滤镜笔刷引擎"

#: ../../reference_manual/brushes/brush_engines/filter_brush_engine.rst:12
msgid "Brush Engine"
msgstr ""

#: ../../reference_manual/brushes/brush_engines/filter_brush_engine.rst:12
msgid "Filters"
msgstr ""

#: ../../reference_manual/brushes/brush_engines/filter_brush_engine.rst:20
msgid ".. image:: images/icons/filterbrush.svg"
msgstr ".. image:: images/icons/filterbrush.svg"

#: ../../reference_manual/brushes/brush_engines/filter_brush_engine.rst:21
msgid ""
"Where in other programs you have a 'dodge tool', 'blur tool' and 'sharpen "
"tool', Krita has a special brush engine for this: The Filter Brush engine. "
"On top of that, due to Krita's great integration of the filters, a huge "
"amount of filters you'd never thought you wanted to use for a drawing are "
"possible in brush form too!"
msgstr ""
"你可能在其他软件中用过单独的“减淡工具”、“模糊工具”和“锐化工具”等特效工具。"
"Krita 把类似的操作交给一种特殊的笔刷引擎处理，这便是滤镜笔刷引擎。顾名思义，"
"这个笔刷引擎让你可以把滤镜效果作为笔刷使用。由于 Krita 自带了大量滤镜，你能通"
"过该引擎实现的效果也非常丰富。大胆地进行实验吧！"

#: ../../reference_manual/brushes/brush_engines/filter_brush_engine.rst:24
msgid "Options"
msgstr "选项"

#: ../../reference_manual/brushes/brush_engines/filter_brush_engine.rst:26
msgid "The filter brush has of course some basic brush-system parameters:"
msgstr "滤镜笔刷引擎也支持一些基本的笔刷选项："

#: ../../reference_manual/brushes/brush_engines/filter_brush_engine.rst:28
msgid ":ref:`option_brush_tip`"
msgstr ":ref:`option_brush_tip`"

#: ../../reference_manual/brushes/brush_engines/filter_brush_engine.rst:29
msgid ":ref:`blending_modes`"
msgstr ":ref:`blending_modes`"

#: ../../reference_manual/brushes/brush_engines/filter_brush_engine.rst:30
msgid ":ref:`option_opacity_n_flow`"
msgstr ":ref:`option_opacity_n_flow`"

#: ../../reference_manual/brushes/brush_engines/filter_brush_engine.rst:31
msgid ":ref:`option_size`"
msgstr ":ref:`option_size`"

#: ../../reference_manual/brushes/brush_engines/filter_brush_engine.rst:32
msgid ":ref:`option_mirror`"
msgstr ":ref:`option_mirror`"

#: ../../reference_manual/brushes/brush_engines/filter_brush_engine.rst:33
msgid ":ref:`option_rotation`"
msgstr ":ref:`option_rotation`"
