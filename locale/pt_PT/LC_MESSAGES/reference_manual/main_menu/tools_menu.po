msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-02-19 15:50+0000\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-POFile-SpellExtra: Krita kritarec\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: ../../reference_manual/main_menu/tools_menu.rst:1
msgid "The tools menu in Krita."
msgstr "O menu de Ferramentas no Krita."

#: ../../reference_manual/main_menu/tools_menu.rst:11
msgid "Macro"
msgstr "Macro"

#: ../../reference_manual/main_menu/tools_menu.rst:11
msgid "Scripts"
msgstr "Programas"

#: ../../reference_manual/main_menu/tools_menu.rst:17
msgid "Tools Menu"
msgstr "O Menu Ferramentas"

#: ../../reference_manual/main_menu/tools_menu.rst:19
msgid "This contains three things."
msgstr "Este contém três elementos."

#: ../../reference_manual/main_menu/tools_menu.rst:22
msgid "Scripting"
msgstr "Programação"

#: ../../reference_manual/main_menu/tools_menu.rst:24
msgid ""
"When you have python scripting enabled and have scripts toggled, this is "
"where most scripts are stored by default."
msgstr ""
"Quando tiver a programação em Python activa e tiver os programas comutados, "
"aqui é onde será gravada a maioria dos programas por omissão."

#: ../../reference_manual/main_menu/tools_menu.rst:27
msgid "Recording"
msgstr "Gravação"

#: ../../reference_manual/main_menu/tools_menu.rst:31
msgid "The recording and macro features are unmaintained and buggy."
msgstr ""
"As funcionalidades de gravação e de macros têm pouca manutenção e alguns "
"erros."

#: ../../reference_manual/main_menu/tools_menu.rst:33
msgid ""
"Record a macro. You do this by pressing start, drawing something and then "
"pressing stop. This feature can only record brush strokes. The resulting "
"file is stored as a \\*.kritarec file."
msgstr ""
"Grava uma macro. Podê-lo-á fazer se carregar em Iniciar, desenhar algo e "
"depois carregar em Parar. Esta funcionalidade só consegue gravar pinceladas. "
"O ficheiro resultante é gravado como um ficheiro \\*.kritarec."

#: ../../reference_manual/main_menu/tools_menu.rst:36
msgid "Macros"
msgstr "Macros"

#: ../../reference_manual/main_menu/tools_menu.rst:38
msgid ""
"Play back or edit a krita rec file. The edit can only change the brush "
"preset on strokes or add and remove filters."
msgstr ""
"Reproduz ou edita um ficheiro de gravação do Krita. A edição só poderá "
"alterar a predefinição dos pincéis nos traços ou adicionar e remover os "
"filtros."
