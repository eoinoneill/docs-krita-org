# SOME DESCRIPTIVE TITLE.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-04-11 11:42+0100\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: Log guilabel Access HDD mesko Memory home ARGB TEMP\n"
"X-POFile-SpellExtra: IPS px MiB AMD program GiB UltraHD SIMD AVX swapping\n"
"X-POFile-SpellExtra: Random Krita FullHD SSD HOME\n"

#: ../../<generated>:1
msgid "Enable Background Cache Generation"
msgstr "Activar a Geração da Cache em Segundo Plano"

#: ../../reference_manual/preferences/performance_settings.rst:1
msgid "Performance settings in Krita."
msgstr "Configuração da performance no Krita."

#: ../../reference_manual/preferences/performance_settings.rst:11
#: ../../reference_manual/preferences/performance_settings.rst:23
msgid "RAM"
msgstr "RAM"

#: ../../reference_manual/preferences/performance_settings.rst:11
#: ../../reference_manual/preferences/performance_settings.rst:57
msgid "Multithreading"
msgstr "Multitarefa"

#: ../../reference_manual/preferences/performance_settings.rst:11
msgid "Preferences"
msgstr "Preferências"

#: ../../reference_manual/preferences/performance_settings.rst:11
msgid "Settings"
msgstr "Configuração"

#: ../../reference_manual/preferences/performance_settings.rst:11
msgid "Performance"
msgstr "Performance"

#: ../../reference_manual/preferences/performance_settings.rst:11
msgid "Memory Usage"
msgstr "Utilização de Memória"

#: ../../reference_manual/preferences/performance_settings.rst:11
msgid "Lag"
msgstr "Atraso"

#: ../../reference_manual/preferences/performance_settings.rst:16
msgid "Performance Settings"
msgstr "Configuração da Performance"

#: ../../reference_manual/preferences/performance_settings.rst:18
msgid ""
":program:`Krita`, as a painting program, juggles a lot of data around, like "
"the brushes you use, the colors you picked, but primarily, each pixel in "
"your image. Due to this, how :program:`Krita` organizes where it stores all "
"the data can really speed up :program:`Krita` while painting, just like "
"having an organized artist's workplace can really speed up the painting "
"process in real life."
msgstr ""
"O :program:`Krita`, como um programa de pintura, faz o tratamento intensivo "
"de um grande bloco de dados, como os pincéis que usa, as cores que escolheu "
"mas, principalmente, cada pixel da sua imagem. Por esse facto, a forma como "
"o :program:`Krita` organiza onde guarda os dados poderá realmente acelerar "
"o :program:`Krita` enquanto pinta, assim como ter o espaço de trabalho de um "
"artista organizado poderá acelerar o processo de pintura na vida real."

#: ../../reference_manual/preferences/performance_settings.rst:20
msgid ""
"These preferences allow you to configure :program:`Krita's` organisation, "
"but all do require you to restart :program:`Krita`, so it can do this "
"organisation properly."
msgstr ""
"Estas preferências permitem-lhe configurar a organização do :program:"
"`Krita'`, mas todas elas obrigam a que reinicie o :program:`Krita`, para que "
"possa fazer esta organização de forma adequada."

#: ../../reference_manual/preferences/performance_settings.rst:25
msgid ""
"RAM, or Random Access Memory, is the memory your computer is immediately "
"using. The difference between RAM and the hard drive memory can be compared "
"to the difference between having files on your desk and having files safely "
"stored away in an archiving room: The files on your desk as much easier to "
"access than the ones in your archive, and it takes time to pull new files "
"from the archive. This is the same for your computer and RAM. Files need to "
"be loaded into RAM before the computer can really use them, and storing and "
"removing them from RAM takes time."
msgstr ""
"A RAM ou Random Access Memory (Memória de Acesso Directo), é a memória que o "
"seu computador usa de imediato. A diferença entre a RAM e a memória do disco "
"rígido poderá ser comparada se pensar em ter os seus ficheiros na sua "
"secretária ou ter os ficheiros em segurança numa sala de arquivo: Os "
"ficheiros na sua secretária são muito mais fáceis de aceder que os que estão "
"no arquivo, e leva algum tempo a extrair ficheiros novos do arquivo. Isto é "
"o mesko para o seu computador e RAM. Os ficheiros têm de ser carregados para "
"a RAM antes de o computador os poder realmente começar a usar e o "
"armazenamento e remoção da RAM leva algum tempo."

#: ../../reference_manual/preferences/performance_settings.rst:27
msgid ""
"These settings allow you to choose how much of your virtual desk you "
"dedicate to :program:`Krita`. :program:`Krita` will then reserve them on "
"start-up. This does mean that if you change any of the given options, you "
"need to restart :program:`Krita` so it can make this reservation."
msgstr ""
"Esta configuração permite-lhe escolher quanta da sua secretária virtual irá "
"dedicar ao :program:`Krita`. O :program:`Krita` irá então reservar esse "
"espaço no arranque. Isto significa que, se alterar alguma das opções "
"indicadas, terá de reiniciar o :program:`Krita` para que possa fazer essa "
"reserva."

#: ../../reference_manual/preferences/performance_settings.rst:29
msgid "Memory Limit"
msgstr "Limite de Memória"

#: ../../reference_manual/preferences/performance_settings.rst:30
msgid ""
"This is the maximum space :program:`Krita` will reserve on your RAM on "
"startup. It's both available in percentages and Bytes, so you can specify "
"precisely. :program:`Krita` will not take up more space than this, making it "
"safe for you to run an internet browser or music on the background."
msgstr ""
"Este é o espaço máximo que o :program:`Krita` irá reservar na sua RAM no "
"arranque. Está disponível em percentagem e em 'bytes'; como tal, poderá "
"defini-los com precisão. O :program:`Krita` não irá ocupar mais espaço do "
"que isto, tornando-se seguro para si executar um navegador da Internet ou "
"música em segundo plano."

#: ../../reference_manual/preferences/performance_settings.rst:32
msgid ""
"A feature for advanced computer users. This allows :program:`Krita` to "
"organize the area it takes up on the virtual working desk before putting its "
"data on there. Like how a painter has a standard spot for their canvas, :"
"program:`Krita` also benefits from giving certain data it uses its place (a "
"memory pool), so that it can find them easily, and it doesn't get lost among "
"the other data (memory fragmentation). It will then also not have to spend "
"time finding a spot for this data."
msgstr ""
"Uma funcionalidade para os utilizadores avançados de computadores. Isto "
"permite ao :program:`Krita` organizar a área que ocupa na secretária "
"virtual, antes de colocar os seus dados nela. Tal como um pintor tem um "
"local-padrão para a sua tela, o :program:`Krita` também beneficia de "
"atribuir um espaço a certos dados (uma área de memória), para que os possa "
"encontrar mais facilmente, não ficando perdido no meio dos outros dados "
"(fragmentação da memória). Deste modo, também não terá de perder tempo a "
"encontrar um local para esses dados."

#: ../../reference_manual/preferences/performance_settings.rst:34
msgid ""
"Increasing this, of course, means there's more space for this type of data, "
"but like how filling up your working desk with only one big canvas will make "
"it difficult to find room for your paints and brushes, having a large "
"internal pool will result in :program:`Krita` not knowing where to put the "
"other non-specific data."
msgstr ""
"Se aumentar isto, obviamente significa que existirá mais espaço para este "
"tipo de dados, mas tal como o preenchimento do espaço da sua secretária com "
"uma tela gigante tornar-se-á difícil de encontrar espaço para as suas tintas "
"e pincéis, ter um bloco interno grande fará com que o :program:`Krita` não "
"saiba onde colocar os outros dados não-específicos."

#: ../../reference_manual/preferences/performance_settings.rst:36
msgid ""
"On the opposite end, not giving your canvas a spot at all, will result in "
"you spending more time looking for a place where you will put the new layer "
"or that reference you just took out of the storage. This happens for :"
"program:`Krita` as well, making it slower."
msgstr ""
"No lado oposto, ao não reservar nenhum espaço para a sua área de desenho, "
"fará com que você perca mais tempo à procura de um local onde irá colocar a "
"nova camada ou essa referência que foi buscar ao armazenamento. Isto também "
"acontece com o :program:`Krita`, tornando-o mais lento."

#: ../../reference_manual/preferences/performance_settings.rst:38
msgid ""
"This is recommended to be a size of one layer of your image, e.g. if you "
"usually paint on the image of 3000x3000x8bit-ARGB, the pool should be "
"something like 36 MiB."
msgstr ""
"Recomenda-se que seja o tamanho de uma camada da sua imagem; p.ex., se "
"normalmente pinta sobre uma imagem de 3000 x 3000 x 8bits-ARGB, o espaço "
"deverá ser algo equivalente a 36 MiB."

#: ../../reference_manual/preferences/performance_settings.rst:39
msgid "Internal Pool"
msgstr "Espaço Interno"

#: ../../reference_manual/preferences/performance_settings.rst:40
msgid ""
"As :program:`Krita` does this on start-up, you will need to restart :program:"
"`Krita` to have this change affect anything."
msgstr ""
"Dado que o :program:`Krita` faz isto no arranque, terá de reiniciar o :"
"program:`Krita` para ter esta alteração a afectar tudo."

#: ../../reference_manual/preferences/performance_settings.rst:42
msgid ""
":program:`Krita` also needs to keep all the Undo states on the virtual desk "
"(RAM). Swapping means that parts of the files on the virtual desk get sent "
"to the virtual archive room. This allows :program:`Krita` to dedicate more "
"RAM space to new actions, by sending old Undo states to the archive room "
"once it hits this limit. This will make undoing a little slower, but this "
"can be desirable for the performance of :program:`Krita` overall. This too "
"needs :program:`Krita` to be restarted."
msgstr ""
"O :program:`Krita` também precisa de manter todos os estados da operação "
"'Desfazer' na secretária virtual (RAM). A paginação ('swapping') significa "
"que partes dos ficheiros na secretária virtual são enviados para a sala de "
"arquivo virtual. Isto permite ao :program:`Krita` dedicar mais RAM às acções "
"novas, enviando os estados antigos do 'Desfazer' para a sala de arquivo, "
"assim que atingir este limite. Isto tornará a operação 'Desfazer' um pouco "
"mais lenta, mas isso pode ser desejável para a performance do :program:"
"`Krita` como um tudo. Isto também obriga a reiniciar o :program:`Krita`."

#: ../../reference_manual/preferences/performance_settings.rst:43
msgid "Swap Undo After"
msgstr "Anulação da Memória Virtual Após"

#: ../../reference_manual/preferences/performance_settings.rst:46
msgid "Swapping"
msgstr "Memória Virtual"

#: ../../reference_manual/preferences/performance_settings.rst:48
msgid "File Size Limit"
msgstr "Limite de Tamanho do Ficheiro"

#: ../../reference_manual/preferences/performance_settings.rst:49
msgid ""
"This determines the limit of the total space :program:`Krita` can take up in "
"the virtual archive room. If :program:`Krita` hits the limit of both the "
"memory limit above, and this Swap File limit, it can't do anything anymore "
"(and will freeze)."
msgstr ""
"Isto determina o limite do espaço total que o :program:`Krita` poderá ocupar "
"na sala de arquivo virtual. Se o :program:`Krita` atingir ambos os limites "
"de memória acima, e com o isso o limite do Ficheiro de Memória Virtual, não "
"conseguirá fazer mais nada (e por isso irá bloquear)."

#: ../../reference_manual/preferences/performance_settings.rst:51
msgid "Swap File Location"
msgstr "Localização do Ficheiro de Memória Virtual"

#: ../../reference_manual/preferences/performance_settings.rst:51
msgid ""
"This determines where the Swap File will be stored on your hard-drive. "
"Location can make a difference, for example, Solid State Drives (SSD) are "
"faster than Hard Disk Drives (HDD). Some people even like to use USB-sticks "
"for the swap file location."
msgstr ""
"Isto define onde será guardado o Ficheiro de Memória Virtual no seu disco "
"rígido. A localização poderá fazer toda a diferença;  por exemplo, os Discos "
"de Estado Sólido (SSD) são mais rápidos que os Discos Rígidos Mecânicos "
"(HDD). Algumas pessoas até preferem usar cartões USB para a localização do "
"ficheiro de memória virtual."

#: ../../reference_manual/preferences/performance_settings.rst:54
msgid "Advanced"
msgstr "Avançado"

#: ../../reference_manual/preferences/performance_settings.rst:59
msgid ""
"Since 4.0, Krita supports multithreading for the animation cache and "
"handling the drawing of brush tips when using the pixel brush."
msgstr ""
"Desde o 4.0, o Krita tem suporte multi-tarefa para a 'cache' de animações e "
"para tratar do desenho das pontas do pincel, quando é usado o pincel de "
"pixels."

#: ../../reference_manual/preferences/performance_settings.rst:61
msgid "CPU Limit"
msgstr "Limite do CPU"

#: ../../reference_manual/preferences/performance_settings.rst:62
msgid "The number of cores you want to allow Krita to use when multithreading."
msgstr ""
"O número de núcleos do CPU que deseja permitir que o Krita use quando "
"executar várias tarefas."

#: ../../reference_manual/preferences/performance_settings.rst:64
msgid "Frame Rendering Clones Limit"
msgstr "Limite de Clones no Desenho da Imagem"

#: ../../reference_manual/preferences/performance_settings.rst:64
msgid ""
"When rendering animations to frames, Krita multithreads by keeping a few "
"copies of the image, with a maximum determined by the number of cores your "
"processor has. If you have a heavy animation file and lots of cores, the "
"copies can be quite heavy on your machine, so in that case try lowering this "
"value."
msgstr ""
"Ao desenhar as animações em imagens separadas, o Krita cria várias tarefas, "
"mantendo algumas cópias da imagem, com um tamanho máximo definido pelo "
"número de núcleos de processamento que o seu processador possui. Se tiver um "
"ficheiro de animação pesado e diversos núcleos, as cópias poderão ser "
"demasiado pesadas na sua máquina; por isso, nesse caso, tente baixar este "
"valor."

#: ../../reference_manual/preferences/performance_settings.rst:67
msgid "Other"
msgstr "Outro"

#: ../../reference_manual/preferences/performance_settings.rst:68
msgid "Limit frames per second while painting."
msgstr "Limitar as imagens por segundo durante a pintura."

#: ../../reference_manual/preferences/performance_settings.rst:69
msgid ""
"This makes the canvas update less often, which means Krita can spend more "
"time calculating other things. Some people find fewer updates unnerving to "
"watch however, hence this is configurable."
msgstr ""
"Isto faz com que a área de desenho se actualize com menor frequência, o que "
"significa que o Krita poderá gastar mais tempo a calcular outras coisas. "
"Algumas pessoas podem achar que é irritante ver menos actualizações; "
"contudo, isto é configurável."

#: ../../reference_manual/preferences/performance_settings.rst:70
msgid "Debug logging of OpenGL framerate"
msgstr "Registo de depuração da taxa de imagens do OpenGL"

#: ../../reference_manual/preferences/performance_settings.rst:71
msgid "Will show the canvas framerate on the canvas when active."
msgstr ""
"Irá mostrar a taxa de imagens na área de desenho, quando estiver activa."

#: ../../reference_manual/preferences/performance_settings.rst:72
msgid "Debug logging for brush rendering speed."
msgstr "Depuração do registo para a velocidade de desenho do pincel."

#: ../../reference_manual/preferences/performance_settings.rst:73
msgid ""
"Will show numbers indicating how fast the last brush stroke was on canvas."
msgstr ""
"Irá mostrar números que indicam quão rápido foi o último traço do pincel na "
"área de desenho."

#: ../../reference_manual/preferences/performance_settings.rst:74
msgid "Disable vector optimizations (for AMD CPUs)"
msgstr "Desactivar as optimizações vectoriais (para os CPU's da AMD)"

#: ../../reference_manual/preferences/performance_settings.rst:75
msgid ""
"Vector optimizations are a special way of asking the CPU to do maths, these "
"have names such as SIMD and AVX. These optimizations can make Krita a lot "
"faster when painting, except when you have an AMD CPU under Windows. There "
"seems to be something strange going on there, so just deactivate them then."
msgstr ""
"As optimizações vectoriais são uma forma especial de pedir ao CPU para fazer "
"contas matemáticas; estas têm nomes como SIMD ou AVX. Estas optimizações "
"poderão fazer com que o Krita corra muito mais depressa a pintar, a menos "
"que tenha um CPU da AMD em Windows. Parece existir algo de estranho aqui, "
"por isso desactive-os aqui."

#: ../../reference_manual/preferences/performance_settings.rst:76
msgid "Enable progress reporting"
msgstr "Activar a comunicação do progresso"

#: ../../reference_manual/preferences/performance_settings.rst:77
msgid ""
"This allows you to toggle the progress reporter, which is a little feedback "
"progress bar that shows up in the status bar when you let Krita do heavy "
"operations, such as heavy filters or big strokes. The red icon next to the "
"bar will allow you to cancel your operation. This is on by default, but as "
"progress reporting itself can take up some time, you can switch it off here."
msgstr ""
"Isto permite-lhe comutar o relatório do progresso, o qual é uma pequena "
"barra de progresso que aparece na barra de estado quando deixar que o Krita "
"faça operações pesadas, como filtros pesados ou traços grandes. O ícone "
"vermelho a seguir à barra permitir-lhe-á cancelar a sua operação. Isto está "
"activo por omissão, mas dado que o progresso em si poderá levar algum tempo, "
"poderá desligá-lo aqui."

#: ../../reference_manual/preferences/performance_settings.rst:79
msgid ""
"This enables performance logging, which is then saved to the ``Log`` folder "
"in your ``working directory``. Your working directory is where the autosave "
"is saved at as well."
msgstr ""
"Isto activa o registo de performance, o qual será então gravado na pasta "
"``Log`` da sua ``pasta de trabalho``. A sua pasta de trabalho será onde é "
"feita também a gravação automática."

#: ../../reference_manual/preferences/performance_settings.rst:81
msgid "Performance logging"
msgstr "Registo da performance"

#: ../../reference_manual/preferences/performance_settings.rst:81
msgid ""
"So for unnamed files, this is the ``$HOME`` folder in Linux, and the ``%TEMP"
"%`` folder in Windows."
msgstr ""
"Igual, mas para ficheiros sem nome; esta é a pasta ``$HOME`` no Linux e a "
"pasta ``%TEMP%`` no Windows."

#: ../../reference_manual/preferences/performance_settings.rst:84
msgid "Animation Cache"
msgstr "'Cache' da Animação"

#: ../../reference_manual/preferences/performance_settings.rst:88
msgid ""
"The animation cache is the space taken up by animation frames in the memory "
"of the computer. A cache in this sense is a cache of precalculated images."
msgstr ""
"A 'cache' de animações é o espaço ocupado pelas imagens de uma dada animação "
"na memória do computador. Uma 'cache', neste sentido, é uma 'cache' de "
"imagens pré-calculadas."

#: ../../reference_manual/preferences/performance_settings.rst:90
msgid ""
"Playing back a video at 25 FPS means that the computer has to precalculate "
"25 images per second of video. Now, video playing software is able to do "
"this because it really focuses on this one single task. However, Krita as a "
"painting program also allows you to edit the pictures. Because Krita needs "
"to be able to do this, and a dedicated video player doesn't, Krita cannot do "
"the same kind of optimizations as a dedicated video player can."
msgstr ""
"Se reproduzir um vídeo a 25 IPS, significa que o computador terá de pré-"
"calcular 25 imagens por cada segundo de vídeo. Agora, as aplicações de "
"reprodução de vídeo são capazes de fazer isto, porque focam-se realmente "
"nesta única tarefa. Contudo, sendo o Krita um programa de pintura, também "
"lhe permite editar as imagens. Dado que o Krita precisa de poder fazer isto, "
"e um leitor de vídeo dedicado não, o Krita não pode fazer o mesmo tipo de "
"optimizações que um leitor de vídeo dedicado fará."

#: ../../reference_manual/preferences/performance_settings.rst:92
msgid ""
"Still, an animator does need to be able to see what kind of animation they "
"are making. To do this properly, we need to decide how Krita will regenerate "
"the cache after the animator makes a change. There's fortunately a lot of "
"different options how we can do this. However, the best solution really "
"depends on what kind of computer you have and what kind of animation you are "
"making. Therefore in this tab you can customize the way how and when the "
"cache is generated."
msgstr ""
"De qualquer forma, um animador precisa de conseguir ver o tipo de animação "
"que estão a criar. Para o fazer de forma adequada, é necessário decidir como "
"é que o Krita irá regenerar a 'cache' depois de o animador fazer uma "
"alteração. Felizmente, existe um conjunto de opções diferentes na forma como "
"poderá ser feito. Contudo, a melhor solução realmente depende do tipo de "
"computador que tem e de qual o tipo de animação que está a criar. Como tal, "
"nesta página, poderá personalizar a forma como e quando será gerada a "
"'cache'."

#: ../../reference_manual/preferences/performance_settings.rst:95
msgid "Cache Storage Backend"
msgstr "Infra-estrutura de Armazenamento da 'Cache'"

#: ../../reference_manual/preferences/performance_settings.rst:98
msgid ""
"Animation frame cache will be stored in RAM, completely without any "
"limitations. This is also the way it was handled before 4.1. This is only "
"recommended for computers with a huge amount of RAM and animations that must "
"show full-canvas full resolution 6k at 25 fps. If you do not have a huge "
"amount (say, 64GiB) of RAM, do *not* use this option (and scale down your "
"projects)."
msgstr ""
"A 'cache' de imagens da animação será guardada na RAM, completamente sem "
"qualquer limite. Esta é também a forma como era tratada antes do 4.1. Isto "
"só é recomendado para os computadores com grandes quantidades de RAM e com "
"as animações e com animações que precisam de mostrar a resolução total do "
"ecrã a 6k e a 25 imagens por segundo. Se não tiver uma quantidade enorme "
"(digamos, à volta de 64GiB) de RAM, *não* use esta opção (e reduza os seus "
"projectos)."

#: ../../reference_manual/preferences/performance_settings.rst:102
msgid ""
"Please make sure your computer has enough RAM *above* the amount you "
"requested in the :guilabel:`General` tab. Otherwise you might face system "
"freezes."
msgstr ""
"Por favor, certifique-se que o seu computador tem RAM suficiente *acima* do "
"valor pedido na página :guilabel:`Geral`. Caso contrário, poderá obter "
"bloqueios do sistema."

#: ../../reference_manual/preferences/performance_settings.rst:104
msgid "For 1 second of FullHD @ 25 FPS you will need 200 extra MiB of Memory"
msgstr ""
"Para 1 segundo de FullHD @ 25 IPS, irá precisar de 200 MiB extra de memória"

#: ../../reference_manual/preferences/performance_settings.rst:105
msgid "In-memory"
msgstr "Na memória"

#: ../../reference_manual/preferences/performance_settings.rst:105
msgid ""
"For 1 second of 4K UltraHD@ 25 FPS, you will need 800 extra MiB of Memory."
msgstr ""
"Para 1 segundo de UltraHD 4K @ 25 IPS, irá precisar de 800 MiB extra de "
"memória."

#: ../../reference_manual/preferences/performance_settings.rst:108
msgid ""
"Animation frames are stored in the hard disk in the same folder as the swap "
"file. The cache is stored in a compressed way. A little amount of extra RAM "
"is needed."
msgstr ""
"As imagens da animação são guardadas no disco rígido, na mesma pasta que o "
"ficheiro de memória virtual. A 'cache' é guardada de forma comprimida. "
"Também é necessária uma pequena quantidade de RAM extra."

#: ../../reference_manual/preferences/performance_settings.rst:110
msgid "On-disk"
msgstr "No disco"

#: ../../reference_manual/preferences/performance_settings.rst:110
msgid ""
"Since data transfer speed of the hard drive is slow, you might want to limit "
"the :guilabel:`Cached Frame Size` to be able to play your video at 25 fps. A "
"limit of 2500 px is usually a good choice."
msgstr ""
"Dado que a velocidade de transferência dos dados do disco rígido é lenta, "
"poderá querer limitar o :guilabel:`Tamanho das Imagens em 'Cache'` para "
"poder reproduzir o seu vídeo a 25 imagens por segundo. Um limite de 2500 px "
"é normalmente uma boa escolha."

#: ../../reference_manual/preferences/performance_settings.rst:113
msgid "Cache Generation Options"
msgstr "Opções de Geração da 'Cache'"

#: ../../reference_manual/preferences/performance_settings.rst:115
msgid "Limit Cached Frame Size"
msgstr "Limitar o Tamanho da Imagem em 'Cache'"

#: ../../reference_manual/preferences/performance_settings.rst:116
msgid ""
"Render scaled down version of the frame if the image is bigger than the "
"provided limit. Make sure you enable this option when using On-Disk storage "
"backend, because On-Disk storage is a little slow. Without the limit, "
"there's a good chance that it will not be able to render at full speed. "
"Lower the size to play back faster at the cost of lower resolution."
msgstr ""
"O desenho de uma versão reduzida da imagem, caso a imagem seja maior que o "
"limite indicado. Certifique-se que activa esta opção ao usar a infra-"
"estrutura de armazenamento No Disco, dado esta infra-estrutura é um bocado "
"lenta. Sem o limite, existe uma boa hipótese de não conseguir desenhar a "
"toda a velocidade. Baixe o tamanho para desenhar mais depressa, às custas de "
"uma menor resolução."

#: ../../reference_manual/preferences/performance_settings.rst:117
msgid "Use Region Of Interest"
msgstr "Usar a Região de Interesse"

#: ../../reference_manual/preferences/performance_settings.rst:118
msgid ""
"We technically only need to use the section of the image that is in view. "
"Region of interest represents that section. When the image is above the "
"configurable limit, render only the currently visible part of it."
msgstr ""
"Tecnicamente só é necessário usar a secção da imagem que está visível. A "
"região de interesse representa essa secção. Quando a imagem estiver acima do "
"limite configurável, desenha apenas a parte visível dela."

#: ../../reference_manual/preferences/performance_settings.rst:120
msgid ""
"This allows you to set whether the animation is cached for playback in the "
"background (that is, when you're not using the computer). Then, when "
"animation is cached when pressing play, this caching will take less long. "
"However, turning off this automatic caching can save power by having your "
"computer work less."
msgstr ""
"Isto permite-lhe definir se a animação fica em 'cache' para a reprodução em "
"segundo plano (isto é, quando não estiver a usar o computador9. Aí, quando a "
"animação ficar em 'cache' ao carregar em Reproduzir, este processo irá levar "
"menos tempo. Contudo, se desligar esta 'cache' automática, poderá poupar "
"energia, fazendo com que o seu computador trabalhe menos."
