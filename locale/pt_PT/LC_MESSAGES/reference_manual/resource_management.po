# SOME DESCRIPTIVE TITLE.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-05-19 23:48+0100\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: en krita guilabel image Kritamouseleft Bundles\n"
"X-POFile-SpellExtra: Brushpreset images filters nomeparcial alt Creating\n"
"X-POFile-SpellExtra: Tags Nomemarca menuselection bundle kbd Enter Krita\n"
"X-POFile-SpellExtra: Kritamousescroll icons mousescroll Manageresources\n"
"X-POFile-SpellExtra: mouseleft brushes resources\n"

#: ../../<rst_epilog>:2
msgid ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: mouseleft"
msgstr ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: botão esquerdo"

#: ../../<rst_epilog>:8
msgid ""
".. image:: images/icons/Krita_mouse_scroll.png\n"
"   :alt: mousescroll"
msgstr ""
".. image:: images/icons/Krita_mouse_scroll.png\n"
"   :alt: deslocamento do rato"

#: ../../reference_manual/resource_management.rst:None
msgid ".. image:: images/resources/Tags-krita.png"
msgstr ".. image:: images/resources/Tags-krita.png"

#: ../../reference_manual/resource_management.rst:None
msgid ".. image:: images/resources/Manageresources.png"
msgstr ".. image:: images/resources/Manageresources.png"

#: ../../reference_manual/resource_management.rst:1
msgid "Overview of Resource Management in Krita."
msgstr "Introdução à Gestão de Recursos no Krita."

#: ../../reference_manual/resource_management.rst:11
#: ../../reference_manual/resource_management.rst:21
msgid "Bundles"
msgstr "Pacotes"

#: ../../reference_manual/resource_management.rst:11
msgid "Resources"
msgstr "Recursos"

#: ../../reference_manual/resource_management.rst:16
msgid "Resource Management"
msgstr "Gestão de Recursos"

#: ../../reference_manual/resource_management.rst:18
msgid ""
"Resources are pluggable bits of data, like brush presets or patterns. Krita "
"has variety of resources and also has a good resource management starting "
"from 2.9, making it really easy for artists to share and collate all the "
"resources together"
msgstr ""
"Os recursos são pedaços modulares de dados, como predefinições de pincéis ou "
"padrões. O Krita tem uma grande variedade de recursos e também tem uma boa "
"gestão de recursos desde a versão 2.9, tornando-se simples para os artistas "
"partilharem e recolher todos os recursos em conjunto"

#: ../../reference_manual/resource_management.rst:23
msgid ""
"Starting from 2.9 Krita has a new format to manage resources it is called "
"''Bundles'', a bundle is just a compressed file containing all the resources "
"together."
msgstr ""
"Desde a versão 2.9, o Krita passou a ter um novo formato para gerir os "
"recursos, que se chama ''Bundles'' (Pacotes), sendo um pacote apenas um "
"ficheiro comprimido com todos os recursos em conjunto."

#: ../../reference_manual/resource_management.rst:26
msgid "Tags"
msgstr "Marcas"

#: ../../reference_manual/resource_management.rst:28
msgid ""
"Krita also has a robust tagging system for you to manage the resources on "
"the fly while painting. All Krita resources can be tagged. These tags can be "
"added via the resource manager, but also via the respective dockers such as "
"brush preset docker, pattern docker etc. You can |mouseleft| the plus icon "
"in the docker and add a tag name. In addition to adding you can rename and "
"delete a tag as well."
msgstr ""
"O Krita também tem um sistema robusto de criação de marcas para poder gerir "
"os recursos na hora enquanto pinta. Todos os recursos do Krita poderão ser "
"marcados. Estas marcas poderão ser adicionadas com o gestor de recursos, mas "
"também com as áreas acopláveis respectivas, como a área de predefinições de "
"pincéis, a área de padrões, etc. Poderá carregar com o |mouseleft| sobre o "
"sinal '+' da área acoplável e adicionar o nome de uma marca. Para além de "
"adicionar marcas, poderá mudar os seus nomes e apagá-las."

#: ../../reference_manual/resource_management.rst:33
msgid ""
"Resources can belong to one or more tags.  For example, you may have a Brush "
"Preset of a favorite **Ink Pen** variant and have it tagged so it shows in "
"up in your Inking, Painting, Comics and Favorites groups of brushes."
msgstr ""
"Os recursos poderão pertencer a uma ou mais marcas. Por exemplo, poderá ter "
"uma Predefinição de Pincel de uma variante favorita da **Caneta de Tinta** e "
"tê-la marcada, para que apareça nos seus grupos de pincéis de Pintura, "
"Tintas, Bandas Desenhadas e Favoritos."

#: ../../reference_manual/resource_management.rst:34
msgid ""
"Brushes in the \"Predefined\" tab of the Brush Settings Editor can be also "
"tagged and grouped for convenience."
msgstr ""
"Os pincéis na página \"Predefinido\" do Editor de Configurações dos Pincéis "
"também podem ser marcados e agrupados por conveniência."

#: ../../reference_manual/resource_management.rst:37
msgid "Filtering"
msgstr "Filtragem"

#: ../../reference_manual/resource_management.rst:40
msgid ""
"Some dockers, for example the brush preset docker as shown below, have a "
"resource filter, which functions like a powerful search bar for the resource "
"in question."
msgstr ""
"Algumas áreas acopláveis, como por exemplo a área de predefinições dos "
"pincéis apresentada em baixo, têm um filtro de recursos, que funciona como "
"uma barra de pesquisa poderosa para o recurso em questão."

#: ../../reference_manual/resource_management.rst:43
msgid ".. image:: images/brushes/Brushpreset-filters.png"
msgstr ".. image:: images/brushes/Brushpreset-filters.png"

#: ../../reference_manual/resource_management.rst:44
msgid ""
"You can enter brush name, tag name to quickly pull up a list of brush preset "
"you want. When you select any tag from the tag drop-down and want to include "
"brush presets from other tags as well then you can add filters the following "
"way:"
msgstr ""
"Poderá indicar o nome do pincel ou o nome da marca para obter rapidamente "
"uma lista das predefinições de pincéis que deseja. Quando seleccionar "
"qualquer marca na lista de marcas e quiser também incluir as predefinições "
"de pincéis das outras marcas, então poderá adicionar filtros da seguinte "
"forma:"

#: ../../reference_manual/resource_management.rst:46
msgid ""
"To filter based on the partial, case insensitive name of the resources you "
"can add ``partialname`` or ``!partialname``"
msgstr ""
"Para filtrar com base no nome parcial e sem distinção entre maiúsculas e "
"minúsculas, poderá adicionar ``nomeparcial`` ou ``!nomeparcial``"

#: ../../reference_manual/resource_management.rst:47
msgid ""
"To include other Tags type the respective name of the tag in square brackets "
"like this ``[Tagname]`` or to exclude a tag type ``![Tagname]``."
msgstr ""
"Para incluir outras marcas, escreva o nome respectivo da marca entre "
"parêntesis rectos, como por exemplo ``[Nomemarca]`` ou para excluir uma "
"marca, escreva ``![Nomemarca]``."

#: ../../reference_manual/resource_management.rst:48
msgid ""
"For case sensitive matching of preset name type ``\"Preset name\"`` or ``! "
"\"Preset name\"`` to exclude."
msgstr ""
"Para a correspondência com distinção de maiúsculas ou minúsculas do nome da "
"predefinição, escreva ``\"Nome da predefinição\"`` ou ``! \"Nome da "
"predefinição\"`` para excluir."

#: ../../reference_manual/resource_management.rst:51
msgid "An incredibly quick way to save a group or brushes into a tag is to:"
msgstr ""
"Uma forma incrivelmente rápida de gravar um grupo ou pincéis individuais "
"numa marca é:"

#: ../../reference_manual/resource_management.rst:53
msgid ""
"Create a new tag by |mouseleft| on the green plus sign.  This will empty out "
"the contents of the Brush Preset docker."
msgstr ""
"Criar uma nova marca com o |mouseleft| sobre o sinal '+' verde. Isto irá "
"esvaziar o conteúdo da área de Predefinições dos Pincéis."

#: ../../reference_manual/resource_management.rst:54
msgid ""
"Use the :guilabel:`Resource Filter` at the bottom of the :guilabel:`Brush "
"Presets` dock or :guilabel:`Brush Settings Editor` to type in what you want "
"to group.  For instance: if you type **Pencil** in the filter box you will "
"get all Brush Presets with **Pencil** somewhere in their name.  Now you have "
"all the Pencil-related Brush Presets together in one place."
msgstr ""
"Use o :guilabel:`Filtro de Recursos`, no fundo da área de :guilabel:"
"`Predefinições dos Pincéis` ou no :guilabel:`Editor de Predefinições de "
"Pincéis`, para escrever o que deseja agrupar. Por exemplo: se escrever "
"**Lápis** no campo de filtragem, irá obter todas as Predefinições de Pincéis "
"com **Lápis** algures no nome. Agora terá todas as Predefinições de Pincéis "
"relacionadas com Lápis num único local."

#: ../../reference_manual/resource_management.rst:55
msgid ""
"To finish, click the :guilabel:`Save` button (small disk icon to the right "
"of the :guilabel:`Resource Filter` box) or press :kbd:`Enter` and all the "
"items will be saved with the new tag you created."
msgstr ""
"Para terminar, carregue no botão :guilabel:`Gravar` (o pequeno ícone da "
"disquete à direita da área do :guilabel:`Filtro de Recursos`) ou carregue "
"em :kbd:`Enter` para que todos os itens sejam gravados com a nova marca "
"criada por si."

#: ../../reference_manual/resource_management.rst:57
msgid ""
"Now, anytime you want to open up your \"digital pencil box\" and see what "
"you have to work with all you have to do is use the pull-down and select :"
"guilabel:`Pencils`.  The Resource Filter works the same way in other parts "
"of Krita so be on the lookout for it!"
msgstr ""
"Agora, sempre que quiser abrir a sua \"caixa de lápis digital\" e ver o que "
"tem ao seu dispor é usar a lista e seleccionar :guilabel:`Lápis`. O Filtro "
"de Recursos funciona da mesma forma que outras partes do Krita, por isso "
"esteja atento a ele!"

#: ../../reference_manual/resource_management.rst:60
msgid "Resource Zooming"
msgstr "Ampliação dos Recursos"

#: ../../reference_manual/resource_management.rst:60
msgid ""
"If you find the thumbnails of the resources such as color swatches brushes "
"and pattern to be small you can make them bigger or :guilabel:`Zoom in`. All "
"resource selectors can be zoomed in and out of, by hovering over the "
"selector and using :kbd:`Ctrl +` |mousescroll|"
msgstr ""
"Se achar que as miniaturas dos recursos, como os quadrados de cores ou dos "
"padrões dos pincéis, são pequenas, podê-las-á tornar maiores, ou seja, :"
"guilabel:`Ampliar`. Todos os selectores de recursos podem ser ampliados ou "
"reduzidos, ao passar o cursor sobre o selector e usando o :kbd:`Ctrl +` |"
"mousescroll|"

#: ../../reference_manual/resource_management.rst:63
msgid "Managing Resources"
msgstr "Gestão dos Recursos"

#: ../../reference_manual/resource_management.rst:65
msgid ""
"As mentioned earlier Krita has a flexible resource management system. "
"Starting from version 2.9 you can share various resources mentioned above by "
"sharing a single compressed zip file created within Krita."
msgstr ""
"Como foi mencionado anteriormente, o Krita tem um sistema flexível de gestão "
"de recursos. A começar pela versão 2.9, poderá partilhar vários recursos "
"mencionados acima, partilhando um único ficheiro ZIP comprimido criado "
"dentro do Krita."

#: ../../reference_manual/resource_management.rst:67
msgid ""
"The manage resources section in the settings was also revamped for making it "
"easy for the artists to prepare these bundle files. You can open manage "
"resource section by going to :menuselection:`Settings`   then :menuselection:"
"`Manage Resources`"
msgstr ""
"A secção de gestão de recursos na configuração foi também remodelada para "
"facilitar aos artistas preparar estes ficheiros de pacotes. Poderá abrir a "
"secção de gestão de recursos se for a :menuselection:`Configuração` e a :"
"menuselection:`Gerir os Recursos`"

#: ../../reference_manual/resource_management.rst:73
msgid "Importing Bundles"
msgstr "Importação de Pacotes"

#: ../../reference_manual/resource_management.rst:75
msgid ""
"To import a bundle click on :guilabel:`Import Bundles/Resources` button on "
"the top right side of the dialog. Select .bundle file format from the file "
"type if it is not already selected, browse to the folder where you have "
"downloaded the bundle, select it and click :guilabel:`Open`. Once the bundle "
"is imported it will be listed in the :guilabel:`Active Bundle` section. If "
"you don't need the bundle you can temporarily make it inactive by selecting "
"it and clicking on the arrow button, this will move it to the :guilabel:"
"`Inactive` section."
msgstr ""
"Para importar um pacote, carregue no botão :guilabel:`Importar Pacotes/"
"Recursos` no canto superior direito da janela. Seleccione o formato de "
"ficheiros .bundle no tipo de ficheiros, caso não esteja já seleccionado, "
"escolha a pasta para onde transferiu o pacote, seleccione-o e carregue em "
"Abrir. Assim que tiver importado o pacote, o mesmo será apresentado na "
"secção do :guilabel:`Pacote Activo`. Se não precisar do pacote, podê-lo-á "
"tornar temporariamente inactivo, seleccionando-o e carregando no botão da "
"seta, sendo que ele irá passar para a secção :guilabel:`Inactivo`."

#: ../../reference_manual/resource_management.rst:79
msgid "Creating your own Bundle"
msgstr "Criar o seu Próprio Pacote"

#: ../../reference_manual/resource_management.rst:81
msgid ""
"You can create your own bundle from the resources of your choice. Click on "
"the :guilabel:`Create bundle` button. This will open a dialog box as shown "
"below"
msgstr ""
"Poderá criar o seu próprio pacote a partir dos recursos à sua escolha. "
"Carregue no botão :guilabel:`Criar um pacote`. O mesmo irá abrir uma janela "
"como aparece abaixo"

#: ../../reference_manual/resource_management.rst:84
msgid ".. image:: images/resources/Creating-bundle.png"
msgstr ".. image:: images/resources/Creating-bundle.png"

#: ../../reference_manual/resource_management.rst:85
msgid ""
"The left hand section is for filling up information about the bundle like "
"author name, website, email, bundle icon, etc. The right hand side provides "
"a list of available resources. Choose the type of resource you wish to add "
"in to the bundle from the drop-down above and add it to the bundle by "
"selecting a resource and clicking on the arrow button."
msgstr ""
"A secção do lado esquerdo serve para preencher informações sobre o pacote, "
"como o nome do autor, a página Web, o e-mail, o ícone do pacote, etc., "
"enquanto o lado direito mostra uma lista com os recursos disponíveis. "
"Escolha o tipo de recurso que deseja adicionar ao grupo na lista acima e "
"adicione-o ao pacote, seleccionando para tal um recurso e carregando no "
"botão da seta."

#: ../../reference_manual/resource_management.rst:90
msgid ""
"Make sure you add brush tips for used in the respective paintop presets you "
"are adding to the bundle. If you don't provide the brush tips then the brush "
"presets loaded from this bundle will have a 'X' mark on the thumbnail "
"denoting that the texture is missing. And the brush preset won't be the same"
msgstr ""
"Certifique-se que adiciona as pontas dos pincéis usadas na predefinição "
"respectiva da operação de pintura que estará a adicionar ao grupo. Se não "
"oferecer as pontas de pincéis, então as predefinições de pincéis carregadas "
"neste pacote terão uma marca 'X' na miniatura, que identifica que a textura "
"está em falta. Por outro lado, a predefinição do pincel não será a mesma"

#: ../../reference_manual/resource_management.rst:92
msgid ""
"Once you have added all the resources you can create bundle by clicking on "
"the :guilabel:`Save` button, the bundle will be saved in the location you "
"have specified. You can then share this bundle with other artists or load it "
"on other workstations."
msgstr ""
"Assim que tiver adicionado todos os recursos, poderá criar um pacote se "
"carregar no botão para :guilabel:`Gravar`, sendo que o pacote será gravado "
"na localização indicada por si. Poderá então partilhar este pacote com os "
"outros artistas ou carregá-lo noutras estações de trabalho."

#: ../../reference_manual/resource_management.rst:95
msgid "Deleting Backup files"
msgstr "Apagar as Cópias de Segurança"

#: ../../reference_manual/resource_management.rst:97
msgid ""
"When you delete a preset from Krita, Krita doesn't actually delete the "
"physical copy of the preset. It just adds it to a black list so that the "
"next time when you start Krita the presets from this list are not loaded. To "
"delete the brushes from this list click on :guilabel:`Delete Backup Files`."
msgstr ""
"Quando necessitar de apagar uma predefinição do Krita, o mesmo não irá "
"apagar de facto a cópia física da predefinição. Simplesmente adiciona-a a "
"uma lista de proibições para que, da próxima vez que iniciar o Krita, as "
"predefinições desta lista não sejam carregadas. Para apagar os pincéis desta "
"lista, carregue no botão para :guilabel:`Apagar as Cópias de Segurança`."

#: ../../reference_manual/resource_management.rst:100
msgid "Deleting Imported Bundles"
msgstr "Apagar os Pacotes Importados"

#: ../../reference_manual/resource_management.rst:102
msgid ""
"In case you wish to delete the bundles you have imported permanently click "
"on the :guilabel:`Open Resource Folder` button in the :guilabel:`Manage "
"Resources` dialog. This will open the resource folder in your file manager / "
"explorer. Go inside the bundles folder and delete the bundle file which you "
"don't need any more. The next time you start Krita the bundle and its "
"associated resources will not be loaded."
msgstr ""
"No caso de desejar apagar de forma permanente os pacotes que tiver "
"importado, carregue no botão :guilabel:`Abrir a Pasta de Recursos` na janela "
"para :guilabel:`Gerir os Recursos`. Isto irá abrir a pasta de recursos no "
"seu gestor / explorador de ficheiros. Vá dentro da pasta de pacotes e apague "
"o ficheiro do pacote que não necessita mais. Da próxima vez que iniciar o "
"Krita, o pacote e os seus recursos associados não serão carregados."

#: ../../reference_manual/resource_management.rst:105
msgid "Resource Types in Krita"
msgstr "Tipos de Recursos no Krita"
