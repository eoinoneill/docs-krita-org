# Translation of docs_krita_org_reference_manual___tools___ellipse.po to Ukrainian
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: docs_krita_org_reference_manual___tools___ellipse\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-05-04 16:42+0300\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 19.03.70\n"

#: ../../<rst_epilog>:28
msgid ""
".. image:: images/icons/ellipse_tool.svg\n"
"   :alt: toolellipse"
msgstr ""
".. image:: images/icons/ellipse_tool.svg\n"
"   :alt: toolellipse"

#: ../../reference_manual/tools/ellipse.rst:None
msgid ".. image:: images/tools/Krita_ellipse_circle.gif"
msgstr ".. image:: images/tools/Krita_ellipse_circle.gif"

#: ../../reference_manual/tools/ellipse.rst:None
msgid ".. image:: images/tools/Krita_ellipse_from_center.gif"
msgstr ".. image:: images/tools/Krita_ellipse_from_center.gif"

#: ../../reference_manual/tools/ellipse.rst:None
msgid ".. image:: images/tools/Krita_ellipse_reposition.gif"
msgstr ".. image:: images/tools/Krita_ellipse_reposition.gif"

#: ../../reference_manual/tools/ellipse.rst:1
msgid "Krita's ellipse tool reference."
msgstr "Довідник з інструмента малювання еліпсів Krita."

#: ../../reference_manual/tools/ellipse.rst:11
msgid "Tools"
msgstr "Інструменти"

#: ../../reference_manual/tools/ellipse.rst:11
msgid "Ellipse"
msgstr "Еліпс"

#: ../../reference_manual/tools/ellipse.rst:11
msgid "Circle"
msgstr "Коло"

#: ../../reference_manual/tools/ellipse.rst:16
msgid "Ellipse Tool"
msgstr "Інструмент «Еліпс»"

#: ../../reference_manual/tools/ellipse.rst:18
msgid "|toolellipse|"
msgstr "|toolellipse|"

#: ../../reference_manual/tools/ellipse.rst:20
msgid ""
"Use this tool to paint an ellipse. The currently selected brush is used for "
"drawing the ellipse outline. Click and hold the left mouse button to "
"indicate one corner of the ‘bounding rectangle’ of the ellipse, then move "
"your mouse to the opposite corner. :program:`Krita` will show a preview of "
"the ellipse using a thin line. Release the button to draw the ellipse."
msgstr ""
"Цим інструментом користуються для малювання еліпса. Для малювання контуру "
"еліпса буде використано поточний вибраний пензель. Натисніть і утримуйте "
"ліву кнопку миші для позначення одного з кутів обмежувального прямокутника "
"еліпса, потім пересуньте вказівник миші у протилежний кут цього "
"прямокутника. :program:`Krita` покаже попередній перегляд контуру еліпса "
"тонкою лінією. Відпустіть кнопку миші, щоб програма намалювала еліпс."

#: ../../reference_manual/tools/ellipse.rst:22
msgid ""
"While dragging the ellipse, you can use different modifiers to control the "
"size and position of your ellipse:"
msgstr ""
"Під час малювання еліпса ви можете використовувати різні модифікатори для "
"керування розміром та розташування еліпса:"

#: ../../reference_manual/tools/ellipse.rst:24
msgid ""
"In order to make a circle instead of an ellipse, hold :kbd:`Shift` while "
"dragging. After releasing :kbd:`Shift` any movement of the mouse will give "
"you an ellipse again:"
msgstr ""
"Щоб створити коло замість еліпса, утримуйте натиснутою клавішу :kbd:`Shift` "
"під час перетягування вказівника. Після того, як ви відпустите клавішу :kbd:"
"`Shift`, будь-який рух вказівником знову призводитиме до малювання еліпса:"

#: ../../reference_manual/tools/ellipse.rst:29
msgid ""
"In order to keep the center of the ellipse fixed and only growing and "
"shrinking the ellipse around it, hold :kbd:`Ctrl` while dragging:"
msgstr ""
"Щоб зафіксувати центр еліпса і малювати еліпс розтягуванням або стисканням "
"еліпса навколо нього, утримуйте натиснутою клавішу :kbd:`Ctrl` під час "
"перетягування вказівника миші:"

#: ../../reference_manual/tools/ellipse.rst:34
msgid "In order to move the ellipse around, hold :kbd:`Alt`:"
msgstr ""
"Для пересування еліпса полотном під час руху вказівником миші натисніть і "
"утримуйте клавішу :kbd:`Alt`:"

#: ../../reference_manual/tools/ellipse.rst:39
msgid ""
"You can change between the corner/corner and center/corner dragging methods "
"as often as you want by holding down or releasing :kbd:`Ctrl`, provided you "
"keep the left mouse button pressed. With :kbd:`Ctrl` pressed, mouse "
"movements will affect all four corners of the bounding rectangle (relative "
"to the center), without :kbd:`Ctrl`, the corner opposite to the one you are "
"moving remains still. With :kbd:`Alt` pressed, all four corners will be "
"affected, but the size stays the same."
msgstr ""
"Ви можете перемикатися між режимами перетягування кут-кут і центр-кут "
"натисканням і відпусканням клавіші :kbd:`Ctrl`, доки ви утримуєте натиснутою "
"ліву кнопку миші. Якщо натиснуто клавішу :kbd:`Ctrl`, пересування вказівника "
"миші пересуватиму усі чотири кути обмежувального прямокутника (відносно "
"центру). Якщо ж клавішу :kbd:`Ctrl` не натиснуто, кут, протилежний до того, "
"який ви рухаєте, лишатиметься нерухомим. Якщо буде натиснуто клавішу :kbd:"
"`Alt`, можна буде пересувати усі чотири кути, але розмір прямокутника "
"лишатиметься незмінним."

#: ../../reference_manual/tools/ellipse.rst:42
msgid "Tool Options"
msgstr "Параметри інструмента"
