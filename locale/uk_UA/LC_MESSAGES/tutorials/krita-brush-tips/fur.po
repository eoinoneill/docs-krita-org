# Translation of docs_krita_org_tutorials___krita-brush-tips___fur.po to Ukrainian
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: docs_krita_org_tutorials___krita-brush-tips___fur\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-05-04 13:43+0300\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 19.03.70\n"

#: ../../tutorials/krita-brush-tips/fur.rst:None
msgid ""
".. image:: images/brush-tips/Krita-brushtips-fur_01.png\n"
"   :alt: Some example of furs and hair"
msgstr ""
".. image:: images/brush-tips/Krita-brushtips-fur_01.png\n"
"   :alt: Приклад шерсті та волосся"

#: ../../tutorials/krita-brush-tips/fur.rst:None
msgid ""
".. image:: images/brush-tips/Krita-brushtips-fur_02.png\n"
"   :alt: brush setting dialog for fur brush"
msgstr ""
".. image:: images/brush-tips/Krita-brushtips-fur_02.png\n"
"   :alt: Вікно параметрів пензля для шерстяного пензля"

#: ../../tutorials/krita-brush-tips/fur.rst:None
msgid ""
".. image:: images/brush-tips/Krita-brushtips-fur_03.png\n"
"   :alt: brush setting dialog for fur"
msgstr ""
".. image:: images/brush-tips/Krita-brushtips-fur_03.png\n"
"   :alt: Вікно параметрів для шерсті"

#: ../../tutorials/krita-brush-tips/fur.rst:None
msgid ""
".. image:: images/brush-tips/Krita-brushtips-fur_04.png\n"
"   :alt: brush setting dialog showing color gradation"
msgstr ""
".. image:: images/brush-tips/Krita-brushtips-fur_04.png\n"
"   :alt: Вікно параметрів пензля із градацією кольорів"

#: ../../tutorials/krita-brush-tips/fur.rst:None
msgid ""
".. image:: images/brush-tips/Krita-brushtips-fur_05.png\n"
"   :alt: result of the brush that we made"
msgstr ""
".. image:: images/brush-tips/Krita-brushtips-fur_05.png\n"
"   :alt: Результат малювання створеним нами пензлем"

#: ../../tutorials/krita-brush-tips/fur.rst:None
msgid ""
".. image:: images/brush-tips/Krita-brushtips-fur_06.png\n"
"   :alt: using the fur brush to make grass and hair"
msgstr ""
".. image:: images/brush-tips/Krita-brushtips-fur_06.png\n"
"   :alt: Використання шерстяного пензля для малювання трави і волосся"

#: ../../tutorials/krita-brush-tips/fur.rst:None
msgid ""
".. image:: images/brush-tips/Krita-brushtips-fur_07.png\n"
"   :alt: fur brush with the color source to gradient and mix option"
msgstr ""
".. image:: images/brush-tips/Krita-brushtips-fur_07.png\n"
"   :alt: Шерстяний пензель і джерелом кольору — градієнтом та змішуванням"

#: ../../tutorials/krita-brush-tips/fur.rst:1
msgid "A tutorial about creating fur in Krita"
msgstr "Підручник із малювання шерсті у Krita"

#: ../../tutorials/krita-brush-tips/fur.rst:13
msgid "Brush-tips:Fur"
msgstr "Кінчики пензлів: Шерсть"

#: ../../tutorials/krita-brush-tips/fur.rst:16
msgid "Question"
msgstr "Питання"

#: ../../tutorials/krita-brush-tips/fur.rst:18
msgid "What brushes are best for fur textures?"
msgstr "Якими пензлями краще користуватися для текстур шерсті?"

#: ../../tutorials/krita-brush-tips/fur.rst:23
msgid ""
"So typically, you see the same logic applied on fur as on regular :ref:"
"`hair`."
msgstr ""
"Типово, шерсті стосується та сама логіка, яка стосується звичайного :ref:"
"`волосся <hair>`."

#: ../../tutorials/krita-brush-tips/fur.rst:25
msgid ""
"However, you can make a brush a little easier by using the Gradient, Mix and "
"HSV options in the :ref:`pixel and color smudge brushes "
"<pixel_brush_engine>`. Basically, what we want to do is have a stroke start "
"dark and then become lighter as we draw with it, to simulate how hair-tips "
"catch more light and look lighter due to being thinner at the ends, while at "
"the base they are frequently more dark."
msgstr ""
"Втім, пензель можна створити і трохи простіше за допомогою параметрів "
"градієнта, змішування та HSV для :ref:`піксельного пензля та пензля "
"розмазування кольорів <pixel_brush_engine>`. В основному, нам потрібно, щоб "
"мазок розпочинався з темного кольору, а потім світлішав уздовж, для "
"створення ефекту потрапляння більшої частки світла на кінчики шерстинок. "
"Крім того шерстинки на кінчиках мають бути світлішими, оскільки є тоншими, а "
"основа шерсті часто має темніший колір."

#: ../../tutorials/krita-brush-tips/fur.rst:30
msgid ""
"Take the :guilabel:`ink_brush_25` and choose under :menuselection:`Brush Tip "
"--> Predefined “A-2 dirty brush”`. Set the spacing to :guilabel:`Auto` and "
"right-click the spacing bar to type in a value between 0.25 and 0.8. Also "
"turn on the :guilabel:`Enable Pen Settings` on flow. Replicate the pressure "
"curve above on the size option. We don’t want the hairs to collapse to a "
"point, hence why the curve starts so high."
msgstr ""
"Виберіть :guilabel:`ink_brush_25` і :menuselection:`Кінчик пензля --> A-2 "
"dirty brush`. Встановіть інтервал :guilabel:`Авто` і клацніть правою кнопкою "
"на смужці інтервалу, щоб ввести значення між 0.25 і 0.8. Також позначте "
"пункт :guilabel:`Увімкнути параметри пера` для потоку. Відтворіть криву "
"тиску, показану вище, для параметра розміру. Нам не потрібно, щоб шерстинки "
"перетворювалися на точку, ось чому крива починається так високо."

#: ../../tutorials/krita-brush-tips/fur.rst:35
msgid ""
"Then activate value and reproduce this curve with the :guilabel:`Distance` "
"or :guilabel:`Fade` sensor. Like how the pressure sensor changes a value "
"(like size) with the amount of pressure you put on the stylus, the distance "
"sensor measures how many pixels your stroke is, and can change an option "
"depending on that. For the HSV sensors: If the curve goes beneath the "
"middle, it’ll become remove from that adjustment, and above the vertical "
"middle it’ll add to that adjustment. So in this case, for the first 100px "
"the brush dab will go from a darkened version of the active paint color, to "
"the active paint color, and then for 100px+ it’ll go from the active color "
"to a lightened version. The curve is an inverse S-curve, because we want to "
"give a lot of room to the mid-tones."
msgstr ""
"Далі, активуйте значення і відтворіть цю криву для датчиків :guilabel:"
"`Відстань` і :guilabel:`Згасання`. Подібно до того, як датчик тиску змінює "
"значення (зокрема розмір) зі зростанням тиску, який ви прикладаєте до стила, "
"датчик відстані вимірює довжину мазка у пікселях із змінює параметр "
"відповідним чином. Для датчиків HSV: якщо крива опускається нижче середини, "
"коригування зменшується, а якщо піднімається вище середини за вертикаллю — "
"збільшується. Отже, у нашому випадку для перших 100 пікселів мазок пензлем "
"проходить шлях від затемненої версії активного кольору малювання до "
"активного кольору малювання, а потім, після 100 пікселів, від активного "
"кольору малювання до його світлішої версії. Крива є оберненою S-кривою, "
"оскільки нам потрібно багато місця для середніх тонів."

#: ../../tutorials/krita-brush-tips/fur.rst:40
msgid ""
"We do the same thing for saturation, so that the darkened color is also "
"slightly desaturated. Notice how the curve is close to the middle: This "
"means its effect is much less strong than the value adjustment. The result "
"should look somewhat like the fifth one from the left on the first row of "
"this:"
msgstr ""
"Ми робимо те саме для насиченості, щоб темніший колір був також дещо "
"зненасиченим. Зауважте, як крива наближається до середини: це означає, що її "
"вплив є набагато меншим за коригування для значення. Результат має виглядати "
"так, як він виглядає для п'ятого малюнка зліва у першому ряду малюнків:"

#: ../../tutorials/krita-brush-tips/fur.rst:45
msgid ""
"The others are done with the smudge brush engine, but a similar setup, "
"though using color rate on distance instead. Do note that it’s very hard to "
"shade realistic fur, so keep a good eye on your form shadow. You can also "
"use this with grass, feathers and other vegetation:"
msgstr ""
"Інші малюнки зроблено за допомогою рушія пензлів розмазування, але із "
"схожими налаштуваннями, хоча було використано залежність зміни кольору від "
"відстані. Зауважте, що не дуже й складно відтінити реалістичну шерсть, тому "
"слідкуйте за тінню від форми. Цією методикою також можна скористатися для "
"малювання трави, пір'я та рослинності:"

#: ../../tutorials/krita-brush-tips/fur.rst:50
msgid ""
"For example, if you use the mix option in the pixel brush, it’ll mix between "
"the fore and background color. You can even attach a gradient to the color "
"smudge brush and the pixel brush. For color smudge, this is just the :"
"guilabel:`Gradient` option, and it’ll use the active gradient. For the pixel "
"brush, set the color-source to :guilabel:`Gradient` and use the mix option."
msgstr ""
"Наприклад, якщо ви скористаєтеся параметром змішування для піксельного "
"пензля, змішування відбуватиметься між кольором переднього плану і тла. Ви "
"навіть можете додати градієнт до пензля розмазування і піксельного пензля. "
"Для пензля розмазування кольору достатньо пункту :guilabel:`Градієнт`, щоб "
"пензель використовував активний градієнт. Для піксельного пензля встановіть "
"джерело кольору :guilabel:`Градієнт` і скористайтеся пунктом змішування."

#: ../../tutorials/krita-brush-tips/fur.rst:55
msgid ""
"On tumblr it was suggested this could be used to do `this tutorial <https://"
"vimeo.com/78183651>`_. Basically, you can also combine this with the lighter "
"color blending mode and wraparound mode to make making grass-textures really "
"easy!"
msgstr ""
"У tumblr припустили, що цією методикою можна скористатися для `цих настанов "
"<https://vimeo.com/78183651>`_. На базовому рівні, ви також можете поєднати "
"цю методику з режимом змішування кольорів «Світліше» і режимом циклічної "
"обробки для створення текстур трави без зайвих зусиль!"
