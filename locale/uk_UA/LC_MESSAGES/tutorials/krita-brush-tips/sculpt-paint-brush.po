# Translation of docs_krita_org_tutorials___krita-brush-tips___sculpt-paint-brush.po to Ukrainian
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: docs_krita_org_tutorials___krita-brush-tips___sculpt-"
"paint-brush\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-05-04 13:39+0300\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 19.03.70\n"

#: ../../<rst_epilog>:4
msgid ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: mouseright"
msgstr ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: mouseright"

#: ../../tutorials/krita-brush-tips/sculpt-paint-brush.rst:None
msgid ""
".. image:: images/brush-tips/Painter-sculpt-brush-01.png\n"
"   :alt: brush setting dialog to get started"
msgstr ""
".. image:: images/brush-tips/Painter-sculpt-brush-01.png\n"
"   :alt: Початкове вікно параметрів пензля"

#: ../../tutorials/krita-brush-tips/sculpt-paint-brush.rst:None
msgid ""
".. image:: images/brush-tips/Painter-sculpt-brush-02.png\n"
"   :alt: remove pressure from opacity parameter and add fade."
msgstr ""
".. image:: images/brush-tips/Painter-sculpt-brush-02.png\n"
"   :alt: Вилучення тиску із параметрів непрозорості і додавання згасання."

#: ../../tutorials/krita-brush-tips/sculpt-paint-brush.rst:None
msgid ""
".. image:: images/brush-tips/Painter-sculpt-brush-03.png\n"
"   :alt: select the Angular church brush tip"
msgstr ""
".. image:: images/brush-tips/Painter-sculpt-brush-03.png\n"
"   :alt: Вибір кутового кінчика пензля"

#: ../../tutorials/krita-brush-tips/sculpt-paint-brush.rst:None
msgid ""
".. image:: images/brush-tips/Painter-sculpt-brush-04.png\n"
"   :alt: opacity parameter in the brush setting"
msgstr ""
".. image:: images/brush-tips/Painter-sculpt-brush-04.png\n"
"   :alt: Параметр непрозорості у параметрах пензля"

#: ../../tutorials/krita-brush-tips/sculpt-paint-brush.rst:None
msgid ""
".. image:: images/brush-tips/Painter-sculpt-brush-05.png\n"
"   :alt: switch off sensors for color rate"
msgstr ""
".. image:: images/brush-tips/Painter-sculpt-brush-05.png\n"
"   :alt: Вимикання датчиків для кольорів"

#: ../../tutorials/krita-brush-tips/sculpt-paint-brush.rst:None
msgid ""
".. image:: images/brush-tips/Painter-sculpt-brush-06.png\n"
"   :alt: brush rotation is enabled"
msgstr ""
".. image:: images/brush-tips/Painter-sculpt-brush-06.png\n"
"   :alt: Увімкнено обертання пензля"

#: ../../tutorials/krita-brush-tips/sculpt-paint-brush.rst:None
msgid ""
".. image:: images/brush-tips/Painter-sculpt-brush-07.png\n"
"   :alt: result from the brush we made."
msgstr ""
".. image:: images/brush-tips/Painter-sculpt-brush-07.png\n"
"   :alt: Результат малювання створеним нами пензлем."

#: ../../tutorials/krita-brush-tips/sculpt-paint-brush.rst:1
msgid ""
"Tutorial for making sculpt brush like sinix's paint like a sculptor video"
msgstr ""
"Підручник зі створення пензля для скульптурного малювання, подібного до "
"пензля у відео Sinix «Paint like a sculptor»"

#: ../../tutorials/krita-brush-tips/sculpt-paint-brush.rst:13
msgid "Brush-tips:Sculpt-paint-brush"
msgstr "Кінчики пензлів: Пензель для скульптурного малювання"

#: ../../tutorials/krita-brush-tips/sculpt-paint-brush.rst:16
msgid "Question"
msgstr "Питання"

#: ../../tutorials/krita-brush-tips/sculpt-paint-brush.rst:18
msgid ""
"**How do I make a brush like the one in Sinix's paint-like-a-sculptor video?"
"**"
msgstr ""
"**Як створити пензель, подібний до того, яким користується Sinix у відео "
"«paint-like-a-sculptor»?**"

#: ../../tutorials/krita-brush-tips/sculpt-paint-brush.rst:20
msgid ""
"It's actually quite easy, but most easy to do since Krita 3.0 due a few "
"bugfixes."
msgstr ""
"Насправді, це доволі просто, але набагато простіше з часу появи Krita 3.0, "
"оскільки у цій версії було виправлено декілька вад."

#: ../../tutorials/krita-brush-tips/sculpt-paint-brush.rst:22
msgid ""
"First, select *Basic_Wet* from the default presets, and go into the brush "
"editor with :kbd:`F5`."
msgstr ""
"Спочатку, виберіть *Basic_Wet* у типових наборах і увімкніть редагування "
"пензля натисканням клавіші :kbd:`F5`."

#: ../../tutorials/krita-brush-tips/sculpt-paint-brush.rst:27
msgid ""
"Then, the trick is to go into **Opacity**, untoggle **Pressure** from the "
"sensors, toggle **Fade** and then reverse the curve as shown above. Make "
"sure that the curve ends a little above the bottom-right, so that you are "
"always painting something. Otherwise, the smudge won't work."
msgstr ""
"Далі, фокус полягає у тому, щоб перейти до пункту **Непрозорість**, зняти "
"позначку **Тиск** для датчиків, позначити **Згасання**, а потім обернути "
"криву так, як показано нижче. Переконайтеся, що крива завершується трохи над "
"нижнім правим кутом, щоб малювання не припинялося за найменшого тиску. Якщо "
"ви цього не зробите, розмазування не працюватиме."

#: ../../tutorials/krita-brush-tips/sculpt-paint-brush.rst:29
msgid ""
"This'll make the color rate decrease and turn it into a smudge brush as the "
"stroke continues:"
msgstr ""
"Це призведе до зменшення зміни кольорів та перетворить пензель на пензель "
"розмазування наприкінці довгих мазків:"

#: ../../tutorials/krita-brush-tips/sculpt-paint-brush.rst:34
msgid ""
"The **Fade** sensor will base the stroke length on brush size. The "
"**Distance** sensor will base it on actual pixels, and the **Time** on "
"actual seconds."
msgstr ""
"Датчик **Згасання** використовуватиме як основу обчислення довжини мазка "
"розмір пензля. Датчик **Відстань** використовуватиме як основу справжні "
"пікселі, а датчик **Час** — справжні секунди."

#: ../../tutorials/krita-brush-tips/sculpt-paint-brush.rst:36
msgid ""
"Then, select :menuselection:`Brushtip --> Predefined` and select the default "
"*A_Angular_Church_HR* brushtip."
msgstr ""
"Далі, виберіть :menuselection:`Кінчик пензля --> Стандартний` і виберіть "
"типовий кінчик пензля *A_Angular_Church_HR*."

#: ../../tutorials/krita-brush-tips/sculpt-paint-brush.rst:41
msgid "This makes for a nice textured square brush."
msgstr "Так ми створимо квадратний пензель із приємною текстурою."

#: ../../tutorials/krita-brush-tips/sculpt-paint-brush.rst:43
msgid ""
"Of course, this'll make the stroke distance longer to get to smudging, so we "
"go back to the *Opacity*."
msgstr ""
"Звичайно ж, це зробить довжину мазка довшою для переходу до розмазування, "
"отже, ми повернемося до *Непрозорості*."

#: ../../tutorials/krita-brush-tips/sculpt-paint-brush.rst:48
msgid ""
"Just adjust the fade-length by |mouseright| on the slider bar. You can then "
"input a number. In the screenshot, I have 500, but the sweet spot seems to "
"be somewhere between 150 and 200."
msgstr ""
"Просто скоригуйте довжину згасання за допомогою клацання |mouseright| на "
"смужці повзунка. Після клацання ви зможете ввести число. На знімку ми "
"вказали 500, але оптимальне значення може бути десь між 150 і 200."

#: ../../tutorials/krita-brush-tips/sculpt-paint-brush.rst:50
msgid ""
"Now, you'll notice that on the start of a stroke, it might be a little "
"faded, so go into **Color Rate** and turn off the **Enable Pen Settings** "
"there."
msgstr ""
"Тепер, як можна бачити, на початку мазка він є дещо згаслим. Тому йдемо до "
"пункту **Зміна кольору** і вимикаємо **Увімкнути параметри пера**."

#: ../../tutorials/krita-brush-tips/sculpt-paint-brush.rst:55
msgid "Then, finally, we'll make the brush rotate."
msgstr "Далі, нарешті, визначимо обертання пензля."

#: ../../tutorials/krita-brush-tips/sculpt-paint-brush.rst:60
msgid ""
"Tick the **Rotation** parameter, and select it. There, untick **Pressure** "
"and tick **Drawing Angle**."
msgstr ""
"Позначте пункт **Обертання**. Далі, зніміть позначку з пункту **Тиск** і "
"позначте пункт **Кут малювання**."

#: ../../tutorials/krita-brush-tips/sculpt-paint-brush.rst:62
msgid ""
"Then, for better angling, tick **Lock** and set the **Angle Offset** to 90 "
"degrees by |mouseright| the slider bar and typing in 90."
msgstr ""
"Далі, для поліпшення результатів позначте пункт **Заблокувати** і встановіть "
"для пункту **Кутове зміщення** значення 90 градусів, клацнувши на смужці "
"повзунка |mouseright| і ввівши 90 з клавіатури."

#: ../../tutorials/krita-brush-tips/sculpt-paint-brush.rst:64
msgid ""
"Now, give your brush a new name, doodle on the brush-square, **Save to "
"presets** and paint!"
msgstr ""
"Тепер надайте вашому пензлю нової назви, намалюйте ескіз на квадратику "
"нотатника, натисніть **Зберегти до наборів** і малюйте!"
