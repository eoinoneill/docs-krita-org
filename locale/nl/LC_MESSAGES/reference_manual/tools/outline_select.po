# Dutch translations for Krita Manual package
# Nederlandse vertalingen voor het pakket Krita Manual.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Automatically generated, 2019.
# Freek de Kruijf <freekdekruijf@kde.nl>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-09 03:41+0200\n"
"PO-Revision-Date: 2019-05-07 10:13+0200\n"
"Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>\n"
"Language-Team: Dutch <kde-i18n-nl@kde.org>\n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.04.0\n"

#: ../../<generated>:1
msgid "Anti-aliasing"
msgstr "Anti-aliasing"

#: ../../<rst_epilog>:2
msgid ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: mouseleft"
msgstr ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: muislinks"

#: ../../<rst_epilog>:4
msgid ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: mouseright"
msgstr ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: muisrechts"

#: ../../<rst_epilog>:74
msgid ""
".. image:: images/icons/outline_select_tool.svg\n"
"   :alt: toolselectoutline"
msgstr ""
".. image:: images/icons/outline_select_tool.svg\n"
"   :alt: toolselectoutline"

#: ../../reference_manual/tools/outline_select.rst:1
msgid "Krita's outline selection tool reference."
msgstr "Verwijzing naar hulpmiddel Selectie omtrek van Krita."

#: ../../reference_manual/tools/outline_select.rst:13
msgid "Tools"
msgstr "Hulpmiddelen"

#: ../../reference_manual/tools/outline_select.rst:13
msgid "Selection"
msgstr "Selectie"

#: ../../reference_manual/tools/outline_select.rst:13
msgid "Freehand"
msgstr "Vrije hand"

#: ../../reference_manual/tools/outline_select.rst:13
msgid "Outline Select"
msgstr "Selectie van omtrek"

#: ../../reference_manual/tools/outline_select.rst:18
msgid "Outline Selection Tool"
msgstr "Hulpmiddel voor selectie van omtrek"

#: ../../reference_manual/tools/outline_select.rst:20
msgid "|toolselectoutline|"
msgstr "|toolselectoutline|"

#: ../../reference_manual/tools/outline_select.rst:22
msgid ""
"Make :ref:`selections_basics` by drawing freehand around the canvas. Click "
"and drag to draw a border around the section you wish to select."
msgstr ""

#: ../../reference_manual/tools/outline_select.rst:25
msgid "Hotkeys and Sticky keys"
msgstr "Sneltoetsen en plakkende toetsen"

#: ../../reference_manual/tools/outline_select.rst:27
msgid ""
":kbd:`R` sets the selection to 'replace' in the tool options, this is the "
"default mode."
msgstr ""

#: ../../reference_manual/tools/outline_select.rst:28
msgid ":kbd:`A` sets the selection to 'add' in the tool options."
msgstr ""

#: ../../reference_manual/tools/outline_select.rst:29
msgid ":kbd:`S` sets the selection to 'subtract' in the tool options."
msgstr ""

#: ../../reference_manual/tools/outline_select.rst:30
msgid ""
":kbd:`Shift` + |mouseleft| sets the subsequent selection to 'add'. You can "
"release the :kbd:`Shift` key while dragging, but it will still be set to "
"'add'. Same for the others."
msgstr ""

#: ../../reference_manual/tools/outline_select.rst:31
msgid ":kbd:`Alt` + |mouseleft| sets the subsequent selection to  'subtract'."
msgstr ""

#: ../../reference_manual/tools/outline_select.rst:32
msgid ":kbd:`Ctrl` + |mouseleft| sets the subsequent selection to  'replace'."
msgstr ""

#: ../../reference_manual/tools/outline_select.rst:33
msgid ""
":kbd:`Shift + Alt +` |mouseleft| sets the subsequent selection to  "
"'intersect'."
msgstr ""

#: ../../reference_manual/tools/outline_select.rst:37
msgid "Hovering over a selection allows you to move it."
msgstr ""

#: ../../reference_manual/tools/outline_select.rst:38
msgid ""
"|mouseright| will open up a selection quick menu with amongst others the "
"ability to edit the selection."
msgstr ""

#: ../../reference_manual/tools/outline_select.rst:42
msgid ""
"You can switch the behavior of the :kbd:`Alt` key to use :kbd:`Ctrl` instead "
"by toggling the switch in the :ref:`general_settings`"
msgstr ""

#: ../../reference_manual/tools/outline_select.rst:45
msgid "Tool Options"
msgstr "Hulpmiddelopties"

#: ../../reference_manual/tools/outline_select.rst:48
msgid ""
"This toggles whether or not to give selections feathered edges. Some people "
"prefer hard-jagged edges for their selections."
msgstr ""
