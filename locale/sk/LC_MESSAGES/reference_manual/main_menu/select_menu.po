# translation of docs_krita_org_reference_manual___main_menu___select_menu.po to Slovak
# Roman Paholik <wizzardsk@gmail.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: "
"docs_krita_org_reference_manual___main_menu___select_menu\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-04-02 12:17+0200\n"
"Last-Translator: Roman Paholik <wizzardsk@gmail.com>\n"
"Language-Team: Slovak <kde-sk@linux.sk>\n"
"Language: sk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 18.12.3\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"

#: ../../reference_manual/main_menu/select_menu.rst:1
msgid "The select menu in Krita."
msgstr ""

#: ../../reference_manual/main_menu/select_menu.rst:11
#, fuzzy
#| msgid "Grow Selection"
msgid "Selection"
msgstr "Roztiahnuť výber"

#: ../../reference_manual/main_menu/select_menu.rst:16
msgid "Select Menu"
msgstr ""

#: ../../reference_manual/main_menu/select_menu.rst:19
msgid "Select All"
msgstr "Vybrať všetko"

#: ../../reference_manual/main_menu/select_menu.rst:21
msgid "Selects the whole layer."
msgstr ""

#: ../../reference_manual/main_menu/select_menu.rst:22
msgid "Deselect"
msgstr "Zrušiť výber"

#: ../../reference_manual/main_menu/select_menu.rst:24
msgid "Deselects everything (except for active Selection Mask)"
msgstr ""

#: ../../reference_manual/main_menu/select_menu.rst:25
msgid "Reselect"
msgstr "Výber znovu"

#: ../../reference_manual/main_menu/select_menu.rst:27
msgid "Reselects the previously deselected selection."
msgstr ""

#: ../../reference_manual/main_menu/select_menu.rst:28
msgid "Invert Selection"
msgstr "Invertovať výber"

#: ../../reference_manual/main_menu/select_menu.rst:30
msgid "Inverts the selection."
msgstr ""

#: ../../reference_manual/main_menu/select_menu.rst:31
#, fuzzy
#| msgid "Convert Shapes to Vector Selection"
msgid "Convert to Vector Selection."
msgstr "Previesť tvary na vektorový výber"

#: ../../reference_manual/main_menu/select_menu.rst:33
msgid ""
"This converts a raster selection to a vector selection. Any layers of "
"transparency there might have been are removed."
msgstr ""

#: ../../reference_manual/main_menu/select_menu.rst:34
#, fuzzy
#| msgid "Convert Shapes to Vector Selection"
msgid "Convert to Raster Selection."
msgstr "Previesť tvary na vektorový výber"

#: ../../reference_manual/main_menu/select_menu.rst:36
msgid "This converts a vector selection to a raster selection."
msgstr ""

#: ../../reference_manual/main_menu/select_menu.rst:37
msgid "Convert Shapes to Vector Selection"
msgstr "Previesť tvary na vektorový výber"

#: ../../reference_manual/main_menu/select_menu.rst:39
msgid "Convert vector shape to vector selection"
msgstr ""

#: ../../reference_manual/main_menu/select_menu.rst:40
msgid "Convert to shape"
msgstr "Previesť na tvar"

#: ../../reference_manual/main_menu/select_menu.rst:42
msgid "Converts vector selection to vector shape."
msgstr ""

#: ../../reference_manual/main_menu/select_menu.rst:43
msgid "Display Selection"
msgstr "Zobraziť výber"

#: ../../reference_manual/main_menu/select_menu.rst:45
msgid "Display the selection. If turned off selections will be invisible."
msgstr ""

#: ../../reference_manual/main_menu/select_menu.rst:46
msgid "Show Global Selection Mask"
msgstr "Zobraziť globálnu masku výberu"

#: ../../reference_manual/main_menu/select_menu.rst:48
msgid ""
"Shows the global selection as a selection mask in the layers docker. This is "
"necessary to be able to select it for painting on."
msgstr ""

#: ../../reference_manual/main_menu/select_menu.rst:49
msgid "Scale"
msgstr "Mierka"

#: ../../reference_manual/main_menu/select_menu.rst:51
msgid "Scale the selection"
msgstr ""

#: ../../reference_manual/main_menu/select_menu.rst:52
msgid "Select from Color Range"
msgstr "Vybrať z farebného rozsahu"

#: ../../reference_manual/main_menu/select_menu.rst:54
msgid "Select from a certain color range."
msgstr ""

#: ../../reference_manual/main_menu/select_menu.rst:55
msgid "Select Opaque"
msgstr "Zvoliť nepriehľadný"

#: ../../reference_manual/main_menu/select_menu.rst:57
msgid ""
"Select all opaque (non-transparent) pixels in the current active layer. If "
"there's already a selection, this will add the new selection to the old one, "
"allowing you to select the opaque pixels of multiple layers into one "
"selection. Semi-transparent (or semi-opaque) pixels will be semi-selected."
msgstr ""

#: ../../reference_manual/main_menu/select_menu.rst:58
msgid "Feather Selection"
msgstr "Rozotrieť výber"

#: ../../reference_manual/main_menu/select_menu.rst:60
msgid ""
"Feathering in design means to soften sharp borders. So this adds a soft "
"border to the existing selection."
msgstr ""

#: ../../reference_manual/main_menu/select_menu.rst:61
msgid "Grow Selection"
msgstr "Roztiahnuť výber"

#: ../../reference_manual/main_menu/select_menu.rst:63
msgid "Make the selection a few pixels bigger."
msgstr ""

#: ../../reference_manual/main_menu/select_menu.rst:64
msgid "Shrink Selection"
msgstr "Zmrštiť výber"

#: ../../reference_manual/main_menu/select_menu.rst:66
msgid "Make the selection a few pixels smaller."
msgstr ""

#: ../../reference_manual/main_menu/select_menu.rst:67
msgid "Border Selection"
msgstr "Vybrať okraj"

#: ../../reference_manual/main_menu/select_menu.rst:69
msgid ""
"Take the current selection and remove the insides so you only have a border "
"selected."
msgstr ""

#: ../../reference_manual/main_menu/select_menu.rst:70
msgid "Smooth"
msgstr "Plynulý"

#: ../../reference_manual/main_menu/select_menu.rst:72
msgid "Make the selection a little smoother. This removes jiggle."
msgstr ""

#~ msgid "--"
#~ msgstr "--"
